//Name: Brandon Mathis
//File Name: Automata.cpp
//Date: 17 April, 2015
//Program Description: Function definitions for General Automata

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <utility>
using namespace std;

//===============================================================
//Default constructor for Automata class
Automata::Automata()
	{
	initialState = "";
	currentState = "";
	}
//===============================================================
//Add a symbol to the alphabet
void Automata::addToAlphabet(string letter)
	{
	alphabet.insert(alphabet.end(),letter);	//Insert new symbol at end of vector
	}
//===============================================================
//Add a state to the automata
void Automata::addStates(string state)
	{
	states.insert(states.end(),state);	//Insert new state at end of vector
	}
//===============================================================
//Add a final state to the automata
void Automata::addFinalStates(string state)
	{
	finalStates.insert(finalStates.end(),state);	//Insert new final state at end of vector	
	}
//===============================================================
//Set initial state and current state of the automata
void Automata::setInitialState(string state)
	{
	initialState = state;
	currentState = state;
	}
//==============================================================
//Returns the initial state of the automata
string Automata::getInitialState()
	{
	return initialState;	
	}
//==============================================================
//Resets the current state of the automata to the original initial state
void Automata::resetCurrentState()
	{
	currentState = initialState;	
	}
//===============================================================
//Adds a new transition to the automata
void Automata::addTransition(string inputState, string transition, string transitionState)
	{
	pair<string,string> state(inputState,transition);	//Creates a pair containing the input state and the transition it makes
	pair<pair<string,string>,string> transitionTo(state,transitionState);	//Combines previous pair with the state that is transitioned to
	transitionList.insert(transitionTo);	//Adds the transition to the Map of transitions
	}
//===============================================================
//Transitions from current state to new state on the input symbol
void Automata::transition(string input) 
	{
	try
		{	
		pair<string,string> state(currentState,input);
		currentState = transitionList.at(state);			//Transitions if the pair was in the transition list,
															//otherwise it throws an Exception
		cout << " -" << input << "-> ";
		cout << "[" << currentState << "]";
		}
	catch(...)
		{
		cout << " (Invalid symbol " << input << ")";		//The transition was not accepted by the automata
		throw("Not accepted");
		}
	}
//===============================================================
//Returns whether the automata is in a final state or not
bool Automata::checkFinalState()
	{
	int size;
	bool found;
	found = false;
	size = finalStates.size();
	for(int n = 0; n < size && !found; n++)
		{
		if(currentState.compare(finalStates[n]) == 0)		//If the current state of the automata is a final state
			found = true;	
		}
	return found;
	}
//============================================================
//Displays all the information contained in the automata
void Automata::displayAutomata()
	{
	int size;
	cout << "----------D F A----------" << endl;
	cout << "<states>" << endl;
	size = states.size();
	for(int n = 0; n < size; n++)
		cout << states[n] << " ";
	cout << endl;
	cout << "<alphabet>" << endl;
	size = alphabet.size();
	for(int n = 0; n < size; n++)
		cout << alphabet[n] << " ";
	cout << endl;
	cout << "<transitions>" << endl;
	for(map<pair<string,string>,string>::iterator i = transitionList.begin(); i != transitionList.end(); i++)	//Breaks map into current state, 
		cout << "(" << (i -> first).first << "," << (i -> first).second << ") -> " <<  i -> second << endl;		//input symbol, transition to state
	cout << "<initial state>" << endl;
	cout << initialState << endl;
	cout << "<final states" << endl;
	size = finalStates.size();
	for(int n = 0; n < size; n++)
		cout << finalStates[n] << " ";	
	cout << endl;
	cout <<"-------------------------" << endl;
	}