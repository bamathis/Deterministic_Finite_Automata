//Name: Brandon Mathis
//File Name: Automata.h
//Date: 17 April, 2015
//Program Description: Header file for General Automata

#include <vector>
#include <string>
#include <map>
#include <utility>
using namespace std;

class Automata
	{
	private:
		vector<string> states;
		vector<string> alphabet;
		vector<string> finalStates;
		string initialState;
		string currentState;
		map<pair<string,string>,string> transitionList;

	public:
		Automata();
		void addStates(string state);
		void addToAlphabet(string letter);
		void addFinalStates(string state);
		void setInitialState(string state);
		string getInitialState();
		void resetCurrentState();
		void addTransition(string state, string transition,string transitionState);
		void transition(string input);
		bool checkFinalState();
		void displayAutomata();
	};
#include "Automata.cpp"
