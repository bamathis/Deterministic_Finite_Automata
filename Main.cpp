//Name: Brandon Mathis
//File Name: Main.cpp
//Date:  17 April, 2015
//Program Description: Driver file for the Automata class

#include <iostream>
#include<fstream>
#include <stdlib.h>
#include<string.h>
#include "Automata.h"
using namespace std;

void setupAutomata(Automata& automata, string automataInfo, int& automataSection);

int main (int argc, char* argv[])
    {
    ifstream inFile;
    string automataInfo;
    bool final;
    int automataSection;
    int length;
    string inputString;
    string section;
    Automata automata;
    if(argc < 2)                    //If only the program name was given
        {
        cout << "usage: <prog_name> <file_name>" << endl;
        cout << endl;
        exit(1);
        }
    inFile.open(argv[1]);
    if (inFile.fail())               //If the automata file was not found
        {
        cout << "Automata file could not be opened!" << endl;
        cout << endl;
        exit(1);
        }
    automataSection = 0;            //Keeps track of what section of automata should be populated
    while(getline(inFile,automataInfo))
        {
        setupAutomata(automata,automataInfo,automataSection);
        }
    automata.displayAutomata();
    cout << endl;
    inFile.close();
    while(true)
        {
        automata.resetCurrentState();               //Sets the current state to the initial state
        cout << "Enter a string (CTRL^C to end): ";
        getline(cin,inputString);
        cout << endl;
        length = inputString.size();                //The number of input symbols to be processed
        try
        	{
            cout << "[" <<automata.getInitialState() << "]";
        	for(int n = 0; n < length; n++)
        		{
        		section = inputString.at(n);         //Sets section to the next input symbol of the given string
        		automata.transition(section);	    //Processes the input symbols transition,
                                                    //throws exception if there is no transition on that symbol
        		}
        	final = automata.checkFinalState();     //Checks if automata is in a final state after processing all input symbols
            if(final)
        	   cout << " : Accepted" << endl;
            else
               cout << " : Rejected" << endl;
        	}
        catch(...)                                 //There was no transition defined on an input symbol
        	{
    	   cout << " : Rejected" << endl;
        	}
        cout << endl;
        }
    }
//====================================================================
//Adds all states, alphabet symbols, and transitions to the automata
void setupAutomata(Automata& automata,string automataInfo, int& automataSection)
    {
    if(automataInfo.compare("<states>") == 0)               //Found the header for automata states
        automataSection++;
    else if(automataInfo.compare("<alphabet>") == 0)        //Found the header for alphabet of automata
        automataSection++;
    else if(automataInfo.compare("<transitions>") == 0)     //Found the header for transitions for automata
        automataSection++;
    else if(automataInfo.compare("<initial state>") == 0)   //Found the header for initial state of automata
        automataSection++;
    else if(automataInfo.compare("<final states>") == 0)    //Found the header for final states of automata
        automataSection++;
    else                                                    //Currently in a section of the file that populates automata
        {
        if(automataSection == 1)
            automata.addStates(automataInfo);
        else if(automataSection == 2)
            automata.addToAlphabet(automataInfo);
        else if(automataSection == 3)
            {
            int stringNum;
            string state;
            string transition;
            string transitionState;
            int length;
            stringNum = 0;
            state = "";
            transition = "";
            transitionState = "";
            length = automataInfo.size();
            for (int i = 0; i < length; ++i)
                {
                if(stringNum == 0)                          //If still part of the current state
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        state = state + automataInfo[i];
                    else
                        stringNum++;
                    }
                else if(stringNum == 1)                     //If still part of the input symbol
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        transition = transition + automataInfo[i];
                    else
                        stringNum++;
                    }
                else if(stringNum == 2)                     //If still part of the state being transitioned to
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        transitionState = transitionState + automataInfo[i];
                    else
                        break;
                    }
                }
            automata.addTransition(state,transition,transitionState);
            }
        else if(automataSection == 4)
                automata.setInitialState(automataInfo);
        else if(automataSection == 5)
                automata.addFinalStates(automataInfo);
        }
    }