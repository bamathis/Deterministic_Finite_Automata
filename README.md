# Deterministic_Finite_Automata

[Deterministic_Finite_Automata](https://gitlab.com/bamathis/Deterministic_Finite_Automata) is an implementation of a Deterministic Finite Automata that determines whether a string is part of the language.

## Quick Start

### Program Execution

```
$ ./Main.cpp filename
```

### File Input

Input file for the automata should be in the form of the included file **test1.txt**.